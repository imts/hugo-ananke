---
title: "Giloop The Loop"
featured_image: '/images/gohugo-default-sample-hero-image.jpg'
description: "Surveillé par une gargouille 😨"
---

À toi qui es arrivé•e sur ce site, sache qu'il est une ôde à Hugo, aussi bien Victor que
le générateur de [sites statiques](http://gohugo.io).

> “ Ce qui d'abord est gloire à la fin est fardeau. ” - *V. Hugo*
