---
title: Contact
featured_image: "images/notebook.jpg"
omit_header_text: true
description: "J'aimerai tant vous connaître"
type: page
menu: main

---

> “ Les mots sont de la dentelle sur une feuille blanche. ” - *Giloop*

Si vous avez des questions sur ce tutoriel pour la prise en main de Hugo, et bien pourquoi pas me les envoyer en commentaires avec le formulaire de contact ci-dessous.

L'envoi du formulaire est géré par [Formspree](https://formspree.io/), un service simple et gratuit pour l'envoi de formulaires.

{{< form-contact action="https://formspree.io/xknggqeo"  >}}
