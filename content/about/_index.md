---
title: "À propos"
description: "Giloop - développeur, pas que pour le Web"
featured_image: '/images/tinteniac.jpg'
---

{{< figure src="/images/eglise-tinteniac.jpg" title="Notre Dame de Tinténiac" >}}

Au coeur de la douce ville Tinténiac, une puissante église éclaire la nuit de mille couleurs. C'est un peu Disneyland et c'est surtout une ville où il fait bon vivre.

> C'est là que je vis - *Giloop*
